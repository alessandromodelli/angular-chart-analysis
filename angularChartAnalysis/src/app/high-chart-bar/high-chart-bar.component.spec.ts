import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighChartBarComponent } from './high-chart-bar.component';

describe('HighChartBarComponent', () => {
  let component: HighChartBarComponent;
  let fixture: ComponentFixture<HighChartBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HighChartBarComponent]
    });
    fixture = TestBed.createComponent(HighChartBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
