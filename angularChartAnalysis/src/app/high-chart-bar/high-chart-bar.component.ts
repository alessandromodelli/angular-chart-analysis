import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ChartData } from '../high-chart/high-chart.component';


@Component({
  selector: 'app-high-chart-bar',
  templateUrl: './high-chart-bar.component.html',
  styleUrls: ['./high-chart-bar.component.css']
})

export class HighChartBarComponent implements AfterViewInit, OnInit {

  //@Input() data: ChartData[] = [];

  public optionsChart: Highcharts.Options = {}

  public optionsChart1: Highcharts.Options = {}

  public optionsChart2: Highcharts.Options = {}

  public optionsChart3: Highcharts.Options = {}

  public data = [
    { name: 'US', value: 311591917 },
    { name: 'Canada', value: 34482779 },
    { name: 'Mexico', value: 112336538 },
    { name: 'UK', value: 62641000 },
  ]

  public title = "World Populations";

  ngOnInit(): void {
    this.optionsChart = {
      chart: {
        type: 'column'
      },
      title: {
        text: this.title
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Population (millions)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Population in 2021: <b>{point.y:.1f} millions</b>'
      },
      series: [{
        type: 'column',
        name: 'Population',
        colorByPoint: true,
        groupPadding: 0,
        data: this.data.map(data => { return [data.name, data.value] }),

      }]
    }

    this.optionsChart1 = {
      chart: {
        type: 'column'
      },
      title: {
        text: this.title
      },

      xAxis: {
        categories: this.data.map(data => data.name),
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Rainfall (mm)'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [{
        type:"column",
        name: '2013',
        data: this.data.map(data => data.value * 3)
      }, {
        type:"column",
        name: '2014',
        data: this.data.map(data => data.value * 2)
      }, {
        type:"column",
        name: '2015',
        data: this.data.map(data => data.value)
      }]
    };

    this.optionsChart2 = {
      chart: {
        type: 'bar'
      },
      title: {
        text: this.title,
        align: 'center'
      },
      xAxis: {
        categories: this.data.map(data=> data.name),
        title: {
          text: null
        },
        gridLineWidth: 1,
        lineWidth: 0
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Population (millions)',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        },
        gridLineWidth: 0
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          borderRadius: '50%',
          dataLabels: {
            enabled: true
          },
          groupPadding: 0.1
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        type:"bar",
        name: '2013',
        data: this.data.map(data => data.value * 3)
      }, {
        type:"bar",
        name: '2014',
        data: this.data.map(data => data.value * 2)
      }, {
        type:"bar",
        name: '2015',
        data: this.data.map(data => data.value)
      },
      {
        type:"bar",
        name: '2015',
        data: this.data.map(data => data.value * 4)
      }]
    }

    this.optionsChart3 = {
      chart: {
        type: 'bar'
      },
      title: {
        text: this.title
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Population (millions)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Population in 2021: <b>{point.y:.1f} millions</b>'
      },
      series: [{
        type: 'column',
        name: 'Population',
        colorByPoint: true,
        groupPadding: 0,
        data: this.data.map(data => { return [data.name, data.value] }),

      }]
    }
  }

  ngAfterViewInit() {
    Highcharts.chart('container', this.optionsChart);
    Highcharts.chart('container1', this.optionsChart1);
    Highcharts.chart('container2', this.optionsChart2);
    Highcharts.chart('container3', this.optionsChart3);
  }
}