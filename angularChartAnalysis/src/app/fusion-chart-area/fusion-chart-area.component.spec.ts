import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionChartAreaComponent } from './fusion-chart-area.component';

describe('FusionChartAreaComponent', () => {
  let component: FusionChartAreaComponent;
  let fixture: ComponentFixture<FusionChartAreaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FusionChartAreaComponent]
    });
    fixture = TestBed.createComponent(FusionChartAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
