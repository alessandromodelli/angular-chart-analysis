import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionChartBarComponent } from './fusion-chart-bar.component';

describe('FusionChartBarComponent', () => {
  let component: FusionChartBarComponent;
  let fixture: ComponentFixture<FusionChartBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FusionChartBarComponent]
    });
    fixture = TestBed.createComponent(FusionChartBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
