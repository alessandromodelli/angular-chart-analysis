import { Component } from '@angular/core';

@Component({
  selector: 'app-fusion-chart-bar',
  templateUrl: './fusion-chart-bar.component.html',
  styleUrls: ['./fusion-chart-bar.component.css']
})
export class FusionChartBarComponent {


  barChart1: Object;
  barChart2: Object;
  barChart3: Object;
  barChart4: Object;

  public data = [
    { name: 'US', value: 311591917 },
    { name: 'Canada', value: 34482779 },
    { name: 'Mexico', value: 112336538 },
    { name: 'UK', value: 62641000 },
  ]

  public title = "World Populations";


  constructor() {
    //STEP 2 - Chart Data

    // STEP 3 - Chart Configuration
    const dataSource1 = {
      chart: {
        caption: this.title,
        subCaption: "2013",
        xAxisName: "Month",
        yAxisName: "Revenues (In USD)",
        theme: "fusion"
      },
      data: this.data.map(data=> {return {label: data.name, value: data.value}})
    }

    const dataSource4 = {
      "chart": {
          "caption": "Top 5 Stores by Sales",
          "subCaption": "Last month",
          "yAxisName": "Sales (In USD)",
          "numberPrefix": "$",
          "theme": "fusion"
      },
      "data": this.data.map(data=>{return {label: data.name, value: data.value}})
  }

  const dataSource3 = {
    "chart": {
        "caption": this.title,
        "xAxisname": "Quarter",
        "yAxisName": "Revenues (In USD)",
        "numberPrefix": "$",
        "plotFillAlpha": "80",
        "theme": "fusion"
    },
    "categories": [
        {
            "category": this.data.map(data=> {return {label: data.name}})
        }
    ],
    "dataset": [
        
        {
            "seriesname": "2015",
            "data": this.data.map(data=> {return {value: data.value}})
        },
        {
            "seriesname": "2016",
            "data": this.data.map(data=> {return {value: data.value * 4}})
        },
        {
            "seriesname": "2015",
            "data": this.data.map(data=> {return {value: data.value}})
        },
        {
            "seriesname": "2016",
            "data": this.data.map(data=> {return {value: data.value * 4}})
        }
    ],

}

const dataSource2 = {
  "chart": {
      "caption": this.title,
      "subCaption": "In top 5 stores last month",
      "numberPrefix": "$",
      "placevaluesInside": "1",
      "xAxisLineColor": "#999999",
      "theme": "fusion"
  },
  "categories": [
      {
          "category": this.data.map(data=> {return {label: data.name}})
      }
  ],
  "dataset": [
      {
          "seriesname": "2013",
          "data": this.data.map(data=> {return {value: data.value * 3}})
      },
      {
          "seriesname": "2014",
          "data": this.data.map(data=> {return {value: data.value * 2}})
      }
  ],
}

    this.barChart1 = dataSource1;
    this.barChart2 = dataSource2;
    this.barChart3 = dataSource3;
    this.barChart4 = dataSource4;
  }
}
