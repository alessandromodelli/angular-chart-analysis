import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyChartLineComponent } from './any-chart-line.component';

describe('AnyChartLineComponent', () => {
  let component: AnyChartLineComponent;
  let fixture: ComponentFixture<AnyChartLineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnyChartLineComponent]
    });
    fixture = TestBed.createComponent(AnyChartLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
