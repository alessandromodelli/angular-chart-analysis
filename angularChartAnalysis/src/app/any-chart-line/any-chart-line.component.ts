import { Component } from '@angular/core';
import 'anychart'

@Component({
  selector: 'app-any-chart-line',
  templateUrl: './any-chart-line.component.html',
  styleUrls: ['./any-chart-line.component.css']
})
export class AnyChartLineComponent {

  public chart = anychart.line();

  public chart1 = anychart.line3d();

  public chart2 = anychart.line();

  public chart3 = anychart.line3d();

  public data = [
    { name: "Jan", value: 28 },
    { name: "Feb", value: 29 },
    { name: "Mar", value: 33 },
    { name: "Apr", value: 36 },
    { name: "May", value: 23 },
    { name: "Jun", value: 23 },
    { name: "Jul", value: 28 }
];
// const years = ['2012', '2013', '2014', '2015', '2016'];
public title = 'Average High & Low Temperature'


  drawChart1(container: string) {
    const data = this.data.map(data => {return [data.name,data.value]})
    // create a chart
    //chart = anychart.bar();

    this.chart.title(this.title)
    this.chart.xAxis().title('Month');
    this.chart.yAxis().title('Temperature');
    // create a bar series and set the data
    this.chart.line(data);

    // set the container id
    this.chart.container(container);

    // initiate drawing the chart
    this.chart.draw();
  }

  drawChart2(container: string) {
    const data1 = this.data.map(data => {return [data.name,data.value]})

    const data2 = this.data.map(data => {return [data.name,data.value - 10]})

    const series1 = this.chart1.line(data1);

    // configure the visual settings of the first series
    series1.normal().fill("orange", 0.3);
    series1.hovered().fill("orange", 0.1);
    series1.selected().fill("orange", 0.5);
    series1.normal().stroke("orange", 1, "10 5", "round");
    series1.hovered().stroke("orange", 2, "10 5", "round");
    series1.selected().stroke("orange", 4, "10 5", "round");

    // create the second series
    const series2 = this.chart1.line2d(data2);

    // configure the visual settings of the second series
    series2.normal().fill("#0066cc", 0.3);
    series2.hovered().fill("#0066cc", 0.1);
    series2.selected().fill("#0066cc", 0.5);
    series2.normal().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.hovered().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.selected().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.normal().stroke("#0066cc");
    series2.hovered().stroke("#0066cc", 2);
    series2.selected().stroke("#0066cc", 4);

    this.chart1.title(this.title)
    this.chart1.yAxis().title('Temperature');


    // set the container id
    this.chart1.container(container);

    // initiate drawing the chart
    this.chart1.draw();


  }

  drawChart3(container: string) {
    const data = this.data.map(data => {return {x: data.name, value: data.value}})

    // create a chart
    this.chart2 = anychart.line();

    // create a spline series and set the data
    this.chart2.stepLine(data);
    this.chart2.title(this.title)
    this.chart2.yAxis().title('Temperature');
    // set the container id
    this.chart2.container(container);

    // initiate drawing the chart
    this.chart2.draw();
  }

  drawChart4(container: string) {
    const data1 = this.data.map(data => {return [data.name, data.value * 3]})

    const data2 = this.data.map(data => {return [data.name, data.value * 2]})
    const data3 = this.data.map(data => {return [data.name, data.value]})

    const series1 = this.chart3.line(data1);
    const series2 = this.chart3.line(data2);
    const series3 = this.chart3.line(data3);

    this.chart3.title(this.title)
    this.chart3.yAxis().title('Temperature');


    // set the container id
    this.chart3.container(container);

    // initiate drawing the chart
    this.chart3.draw();


  }

  ngOnInit(): void {

    this.drawChart1("anyChartLine1");
    this.drawChart2("anyChartLine2");
    this.drawChart3("anyChartLine3");
    this.drawChart4("anyChartLine4");

  }
}
