import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionChartLineComponent } from './fusion-chart-line.component';

describe('FusionChartLineComponent', () => {
  let component: FusionChartLineComponent;
  let fixture: ComponentFixture<FusionChartLineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FusionChartLineComponent]
    });
    fixture = TestBed.createComponent(FusionChartLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
