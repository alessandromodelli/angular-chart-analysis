import { Component } from '@angular/core';

@Component({
    selector: 'app-fusion-chart-line',
    templateUrl: './fusion-chart-line.component.html',
    styleUrls: ['./fusion-chart-line.component.css']
})
export class FusionChartLineComponent {

    chart1: Object;
    chart2: Object;
    chart3: Object;
    chart4: Object;

    public data = [
        { name: "Jan", value: 28 },
        { name: "Feb", value: 29 },
        { name: "Mar", value: 33 },
        { name: "Apr", value: 36 },
        { name: "May", value: 23 },
        { name: "Jun", value: 23 },
        { name: "Jul", value: 28 }
    ];
    // const years = ['2012', '2013', '2014', '2015', '2016'];
    public title = 'Average High & Low Temperature'


    constructor() {

        const dataSource1 = {
            "chart": {
                "caption": this.title,
                "subCaption": "2013",
                "xAxisName": "Month",
                "yAxisName": "Temperature",
                "lineThickness": "2",
                "theme": "fusion"
            },
            "data": this.data.map(data => { return { label: data.name, value: data.value } }),

        }

        const dataSource2 = {
            "chart": {
                "caption": this.title,
                "subCaption": "2013 High Low",
                "xAxisName": "Month",
                yAxisName: "Temperature",
                "theme": "fusion"
            },
            "categories": [
                {
                    "category": this.data.map(data => { return { label: data.name } })
                }
            ],
            "dataset": [
                {
                    "seriesname": "2013 - High",
                    "data": this.data.map(data => {return {value: data.value}})
                },
                {
                    "seriesname": "2013 - Low",
                    "data": this.data.map(data => {return {value: data.value - 10}})
                }
            ]
        }

        const dataSource3 = {
            "chart": {
                "caption": this.title,
                "subCaption": "2013",
                "xAxisName": "Month",
                "yAxisName": "Temperature",
                "lineThickness": "2",
                "theme": "fusion"
            },
            "categories": [
                {
                    "category": this.data.map(data => { return { label: data.name } })
                }
            ],
            "data": this.data.map(data => { return {value: data.value } }),

        }

        const dataSource4 = {
            "chart": {
                "caption": this.title,
                "subCaption": "2013 High Low",
                "xAxisName": "Month",
                yAxisName: "Temperature",
                "theme": "fusion"
            },
            "categories": [
                {
                    "category": this.data.map(data => { return { label: data.name } })
                }
            ],
            "dataset": [
                {
                    "seriesname": "2014",
                    "data": this.data.map(data => {return {value: data.value * 3}})
                },
                {
                    "seriesname": "2015",
                    "data": this.data.map(data => {return {value: data.value * 2}})
                },
                {
                    "seriesname": "2016",
                    "data": this.data.map(data => {return {value: data.value }})
                }
            ]
        }

        this.chart1 = dataSource1;
        this.chart2 = dataSource2;
        this.chart3 = dataSource3;
        this.chart4 = dataSource4;
    }
}
