import { Component } from '@angular/core';

@Component({
  selector: 'app-fusion-chart-pie',
  templateUrl: './fusion-chart-pie.component.html',
  styleUrls: ['./fusion-chart-pie.component.css']
})
export class FusionChartPieComponent {

  chart1: Object;
  chart2: Object;
  chart3: Object;
  chart4: Object;

  public data = [
    { value: 335, name: 'Direct' },
    { value: 310, name: 'Email' },
    { value: 274, name: 'Union Ads' },
    { value: 235, name: 'Video Ads' },
    { value: 400, name: 'Search Engine' }
];
public title = "Traffic sources"


  constructor() {

    const dataSource1 = {
      "chart": {
          "caption": this.title,
          "subCaption": "Last year",
          "use3DLighting": "0",
          "showPercentValues": "1",
          "decimals": "1",
          "useDataPlotColorForLabels": "1",
          "theme": "fusion",
          "showlegend": "false",
          tooltipborderthickness: "3",
          pieRadius: 90,
          manageLabelOverflow: true
      },
      "data": this.data.map(data => {return {label: data.name, value: data.value}})
  }

    const dataSource2 = {
        "chart": {
            "caption": this.title,
            "subCaption": "Last year",
            "enableSmartLabels": "0",
            "startingAngle": "0",
            "showPercentValues": "1",
            "decimals": "1",
            "useDataPlotColorForLabels": "1",
            "theme": "fusion"
        },
        "data": this.data.map(data => {return {label: data.name, value: data.value}})
    }

  const dataSource3 = {
    "chart": {
        "caption": this.title,
        "subCaption": "Last year",
        "numberPrefix": "$",
        "defaultCenterLabel": "Total revenue: $64.08K",
        "centerLabel": "Revenue from $label: $value",
        "decimals": "0",
        "theme": "fusion"
    },
    "data": this.data.map(data => {return {label: data.name, value: data.value}})
}

const dataSource4 = {
  "chart": {
      "caption": this.title,
      "subCaption": "Last year",
      "numberPrefix": "$",
      "decimals": "0",
      "theme": "fusion",
      "showlegend": "false",
      "useDataPlotColorForLabels": "1",
      pieRadius: 90,
  },
  "data": this.data.map(data => {return {label: data.name, value: data.value}})
}

    this.chart1 = dataSource1;
    this.chart2 = dataSource2;
    this.chart3 = dataSource3;
    this.chart4 = dataSource4;
  }
}
