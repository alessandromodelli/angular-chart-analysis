import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionChartPieComponent } from './fusion-chart-pie.component';

describe('FusionChartPieComponent', () => {
  let component: FusionChartPieComponent;
  let fixture: ComponentFixture<FusionChartPieComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FusionChartPieComponent]
    });
    fixture = TestBed.createComponent(FusionChartPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
