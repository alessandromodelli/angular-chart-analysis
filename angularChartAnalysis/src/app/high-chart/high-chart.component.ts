// import { Component, Inject, Input, OnInit } from '@angular/core';
import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
// import { Data } from '../read-csv/read-csv.component';
import { DataService } from '../data-service';

@Component({
  selector: 'app-high-chart',
  templateUrl: './high-chart.component.html',
  styleUrls: ['./high-chart.component.css']
})

export class HighChartComponent{

  public data: ChartData[] = [];

}

export class ChartData {
  name: number
  uv: number

  constructor(name: number, uv: number) {
    this.name = name;
    this.uv = uv;

  }
}