import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyChartAreaComponent } from './any-chart-area.component';

describe('AnyChartAreaComponent', () => {
  let component: AnyChartAreaComponent;
  let fixture: ComponentFixture<AnyChartAreaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnyChartAreaComponent]
    });
    fixture = TestBed.createComponent(AnyChartAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
