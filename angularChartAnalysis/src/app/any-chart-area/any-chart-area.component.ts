import { Component } from '@angular/core';

@Component({
  selector: 'app-any-chart-area',
  templateUrl: './any-chart-area.component.html',
  styleUrls: ['./any-chart-area.component.css']
})
export class AnyChartAreaComponent {

  public chart = anychart.area();

  public chart1 = anychart.area3d();

  public chart2 = anychart.verticalArea();

  public chart3 = anychart.area3d();

  public data = [
    { name: "Jan", value: 28 },
    { name: "Feb", value: 29 },
    { name: "Mar", value: 33 },
    { name: "Apr", value: 36 },
    { name: "May", value: 23 },
    { name: "Jun", value: 23 },
    { name: "Jul", value: 28 }
  ];
  // const years = ['2012', '2013', '2014', '2015', '2016'];
  public title = 'Average High & Low Temperature'



  drawChart1(container: string) {
    const data = this.data.map(data => { return [data.name, data.value] })

    // create a chart
    //chart = anychart.bar();

    this.chart.title(this.title)
    // create a bar series and set the data
    this.chart.area(data);

    // set the container id
    this.chart.container(container);

    // initiate drawing the chart
    this.chart.draw();
  }

  drawChart2(container: string) {
    const data1 = this.data.map(data => { return [data.name, data.value] })

    const data2 = this.data.map(data => { return [data.name, data.value - 10] })

    this.chart1.title(this.title).enabled(true)

    const series1 = this.chart1.area(data1);

    const series2 = this.chart1.area(data2);

    // set the container id
    this.chart1.container(container);
    // initiate drawing the chart
    this.chart1.draw();
  }

  drawChart3(container: string) {
    // create a data set
    // const data = anychart.data.set(this.data.map(data => {return [data.name, data.value]}));
    const data = anychart.data.set(this.data.map(data => { return [data.name, data.value + 20, data.value + 10, data.value, data.value - 10] }))

    const data1 = data.mapAs({ x: 0, value: 1 });
    const data2 = data.mapAs({ x: 0, value: 2 });
    const data3 = data.mapAs({ x: 0, value: 3 });
    const data4 = data.mapAs({ x: 0, value: 4 });

    this.chart2.splineArea(data1);
    this.chart2.splineArea(data2);
    this.chart2.splineArea(data3);
    this.chart2.splineArea(data4);

    this.chart2.yScale().stackMode("percent");
    // create a column series and set the data
    this.chart2.tooltip().format("{%yPercentOfCategory}{decimalsCount:2}%");

    this.chart2.title(this.title);
    // set the container id
    this.chart2.container(container);

    // initiate drawing the chart
    this.chart2.draw();
  }

  drawChart4(container: string) {
    const data = this.data.map(data => { return [data.name, data.value + 10, data.value, data.value - 10] });

    this.chart3.data(data);
    this.chart3.yScale().stackMode("value");
    this.chart3.title("3D Stacked Area Chart");

    // set axes titles
    this.chart3.xAxis().title("Month");
    this.chart3.yAxis().title("Sales");

    // set the container id
    this.chart3.container(container);

    // initiate drawing the chart
    this.chart3.draw();


  }

  ngOnInit(): void {

    this.drawChart1("anyChartArea1");
    this.drawChart2("anyChartArea2");
    this.drawChart3("anyChartArea3");
    this.drawChart4("anyChartArea4");

  }
}
