import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyChartPieComponent } from './any-chart-pie.component';

describe('AnyChartPieComponent', () => {
  let component: AnyChartPieComponent;
  let fixture: ComponentFixture<AnyChartPieComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnyChartPieComponent]
    });
    fixture = TestBed.createComponent(AnyChartPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
