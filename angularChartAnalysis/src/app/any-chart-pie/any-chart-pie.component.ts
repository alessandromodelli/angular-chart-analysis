import { Component } from '@angular/core';
import 'anychart'

@Component({
  selector: 'app-any-chart-pie',
  templateUrl: './any-chart-pie.component.html',
  styleUrls: ['./any-chart-pie.component.css']
})
export class AnyChartPieComponent {

  //public chart = anychart.pie();

  // public chart1 = anychart.pie3d();

  // public chart2 = anychart.pie();

  // public chart3 = anychart.pie3d();
  public data = [
    { value: 335, name: 'Direct' },
    { value: 310, name: 'Email' },
    { value: 274, name: 'Union Ads' },
    { value: 235, name: 'Video Ads' },
    { value: 400, name: 'Search Engine' }
  ];
  public title = "Traffic sources"


  drawChart1(container: string) {
    const data = this.data.map(data => {return {x: data.name, value: data.value}})

    // create a chart
    const chart = anychart.pie(data);

    chart.title(this.title)
    chart.fill("aquastyle");
    // create a bar series and set the data
    //chart.pie(data);

    // set the container id
    chart.container(container);

    // initiate drawing the chart
    chart.draw();
  }

  drawChart2(container: string) {
    const data = this.data.map(data => {return {x: data.name, value: data.value}})


    // create a chart
    const chart = anychart.pie3d(data);

    chart.title(this.title)

    // create a bar series and set the data
    //chart.pie(data);

    // set the container id
    chart.container(container);

    // initiate drawing the chart
    chart.draw();


  }

  drawChart3(container: string) {
    const data = this.data.map(data => {return {x: data.name, value: data.value}})



    // create a chart
    const chart = anychart.pie3d(data);

    chart.title(this.title)
    chart.innerRadius("40%");
    chart.labels().position("outside");
    chart.connectorStroke({ color: "#595959", thickness: 2, dash: "2 2" });
    // create a bar series and set the data
    //chart.pie(data);

    // set the container id
    chart.container(container);

    // initiate drawing the chart
    chart.draw();

  }

  drawChart4(container: string) {
    const data = this.data.map(data => {return {x: data.name, value: data.value}})



    // create a chart
    const chart = anychart.pie(data);

    chart.title(this.title)
    chart.innerRadius("80%");
    chart.fill("aquastyle");
    chart.labels().position("outside");
    // set the position of labels
    // chart.labels().position("inside");

    // set the offset for the labels
    // chart.insideLabelsOffset("-75%");
    // create a bar series and set the data
    //chart.pie(data);

    // set the container id
    chart.container(container);

    // initiate drawing the chart
    chart.draw();

  }

  ngOnInit(): void {

    this.drawChart1("anyChartPie2");
    this.drawChart2("anyChartPie1");
    this.drawChart3("anyChartPie3");
    this.drawChart4("anyChartPie4");

  }
}
