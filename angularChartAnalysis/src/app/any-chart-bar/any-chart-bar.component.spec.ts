import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyChartBarComponent } from './any-chart-bar.component';

describe('AnyChartBarComponent', () => {
  let component: AnyChartBarComponent;
  let fixture: ComponentFixture<AnyChartBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnyChartBarComponent]
    });
    fixture = TestBed.createComponent(AnyChartBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
