import { Component } from '@angular/core';
import 'anychart';

@Component({
  selector: 'app-any-chart-bar',
  templateUrl: './any-chart-bar.component.html',
  styleUrls: ['./any-chart-bar.component.css']
})
export class AnyChartBarComponent {

  public chart = anychart.bar();

  public chart1 = anychart.bar3d();

  public chart2 = anychart.column();

  public chart3 = anychart.column3d();

  public data = [
    { name: 'US', value: 311591917 },
    { name: 'Canada', value: 34482779 },
    { name: 'Mexico', value: 112336538 },
    { name: 'UK', value: 62641000 },
  ]

  public title = "World Populations";



  drawChart1(container: string) {
    const data = this.data.map(data=>{return[data.name, data.value]})

    // create a chart
    //chart = anychart.bar();

    this.chart.title(this.title)
    this.chart.xAxis().title('Employee');
    this.chart.yAxis().title('Salary');
    // create a bar series and set the data
    this.chart.bar(data);

    // set the container id
    this.chart.container(container);

    // initiate drawing the chart
    this.chart.draw();
  }

  drawChart2(container: string) {
    const data1 = this.data.map(data=>{return[data.name, data.value]})
    const data2 = this.data.map(data=>{return[data.name, data.value * 2]})

    const series1 = this.chart1.bar(data1);

    // configure the visual settings of the first series
    series1.normal().fill("orange", 0.3);
    series1.hovered().fill("orange", 0.1);
    series1.selected().fill("orange", 0.5);
    series1.normal().stroke("orange", 1, "10 5", "round");
    series1.hovered().stroke("orange", 2, "10 5", "round");
    series1.selected().stroke("orange", 4, "10 5", "round");

    // create the second series
    const series2 = this.chart1.bar(data2);

    // configure the visual settings of the second series
    series2.normal().fill("#0066cc", 0.3);
    series2.hovered().fill("#0066cc", 0.1);
    series2.selected().fill("#0066cc", 0.5);
    series2.normal().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.hovered().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.selected().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.normal().stroke("#0066cc");
    series2.hovered().stroke("#0066cc", 2);
    series2.selected().stroke("#0066cc", 4);

    this.chart1.title("Employee Salary")
    this.chart1.xAxis().title('Employee');
    this.chart1.yAxis().title('Salary');


    // set the container id
    this.chart1.container(container);

    // initiate drawing the chart
    this.chart1.draw();


  }

  drawChart3(container: string) {
    const data = this.data.map(data=>{return[data.name, data.value]})

    // create a chart
    this.chart2 = anychart.column();

    // create a column series and set the data
    const series = this.chart2.column(data);

    // set the container id
    this.chart2.container(container);

    // initiate drawing the chart
    this.chart2.draw();
  }

  drawChart4(container: string) {
    const data1 = this.data.map(data=>{return[data.name, data.value]})

    const data2 = this.data.map(data=>{return[data.name, data.value * 2]})

    const data3 = this.data.map(data=>{return[data.name, data.value * 3]})

    const series1 = this.chart3.column(data1);

    // configure the visual settings of the first series
    series1.normal().fill("gray", 0.3);
    series1.hovered().fill("gray", 0.1);
    series1.selected().fill("gray", 0.5);
    series1.normal().stroke("gray", 1, "10 5", "round");
    series1.hovered().stroke("gray", 2, "10 5", "round");
    series1.selected().stroke("gray", 4, "10 5", "round");

    // create the second series
    const series2 = this.chart3.column(data2);

    // configure the visual settings of the second series
    series2.normal().fill("#0066cc", 0.3);
    series2.hovered().fill("#0066cc", 0.1);
    series2.selected().fill("#0066cc", 0.5);
    series2.normal().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.hovered().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.selected().hatchFill("forward-diagonal", "#0066cc", 1, 15);
    series2.normal().stroke("#0066cc");
    series2.hovered().stroke("#0066cc", 2);
    series2.selected().stroke("#0066cc", 4);

    // create the second series
    const series3 = this.chart3.column(data3);

    // configure the visual settings of the second series
    series3.normal().fill("orange", 0.3);
    series3.hovered().fill("orange", 0.1);
    series3.selected().fill("orange", 0.5);
    series3.normal().hatchFill("forward-diagonal", "orange", 1, 15);
    series3.hovered().hatchFill("forward-diagonal", "orange", 1, 15);
    series3.selected().hatchFill("forward-diagonal", "orange", 1, 15);
    series3.normal().stroke("orange");
    series3.hovered().stroke("orange", 2);
    series3.selected().stroke("orange", 4);

    this.chart3.title("Employee Salary")
    this.chart3.xAxis().title('Employee');
    this.chart3.yAxis().title('Salary');


    // set the container id
    this.chart3.container(container);

    // initiate drawing the chart
    this.chart3.draw();


  }

  ngOnInit(): void {

    this.drawChart1("anyChartBar1");
    this.drawChart2("anyChartBar2");
    this.drawChart3("anyChartBar3");
    this.drawChart4("anyChartBar4");

  }
}
