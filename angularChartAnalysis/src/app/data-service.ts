import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public dataUrl = 'assets/housing.csv';
  public dataArray: Data[];
  public housingMedianAge: ChartData[] = [
    // {name: 3526, uv: 3},
    // {name: 3526, uv: 5},
    // {name: 3426, uv: 78},
    // {name: 3326, uv: 3},
    // {name: 626, uv: 7},
    // {name: 26, uv: 8},
    // {name: 526, uv: 6},
    // {name: 26, uv: 5},
    // {name: 3526, uv: 45},
    // {name: 3526, uv: 8},
    // {name: 3526, uv: 0},
    // {name: 3526, uv: 2},
    // {name: 3526, uv: 87},
    // {name: 3526, uv: 6},
    // {name: 3526, uv: 3},
    // {name: 3526, uv: 4}
  ];
public prova: {uv: number}[] = [];

  constructor(private http: HttpClient){
    this.dataArray = [];
     http.get('../assets/housing.csv', { responseType: 'text' }).subscribe(
      data => {

        let csvToRowArray = data.split("\n");
        for (let index = 1; index < csvToRowArray.length - 1; index++) {
          let row = csvToRowArray[index].split(",");
          this.dataArray.push(new Data(+row[0], +row[1], +row[2], +row[3], +row[4], +row[5], +row[6], +row[7], +row[8], row[9]));
          //this.housingMedianAge.push(new ChartData(+row[8], +row[2]));
          this.housingMedianAge.push({ name: +row[8], uv: +row[2] });
          this.prova.push({uv: +row[7]});
        }
        // console.log(this.housingMedianAge)
        // this.housingMedianAge = this.housingMedianAge.slice(0,20);
        // console.log(this.housingMedianAge)
        //return this.housingMedianAge;

        //this.housingMedianAge.push(new ChartData())
        this.housingMedianAge = this.dataArray?.map((record) => {
            //console.log(record)
            return ({
              name: record['median_house_value'] ?? 0,
              uv: record['housing_median_age'] ?? 0
            });
          }) ?? [{ name: "", uv: 0 }];
          
      },
      error => {
        console.log(error);
      }
    );

  }

  getData(){
    return this.prova;
  }



  // resolveAfter2Seconds() {
  //   return new Promise(resolve => {
  //     resolve(this.housingMedianAge = this.housingMedianAge.slice(0,20));
  //   });
  // }

  // async getData() {
  //   this.resolveAfter2Seconds().then(value => {
      
  //     console.log(this.housingMedianAge)
  //     //console.log(`promise result: ${value}`);
  //   });
  //   console.log('I will not wait until promise is resolved');
  // }


  // getData(){
  //   this.cutData().then(value => {
  //     console.log(value)
  //   })
  // }

  // cutData(){
  //   //     const data = this.dataArray?.map((record) => {
  //   //   return ({
  //   //     name: record['median_house_value'] ?? 0,
  //   //     uv: record['housing_median_age'] ?? 0
  //   //   });
  //   // }) ?? [{ name: "", uv: 0 }];
  //   // console.log(data)
  //   // const formattedData = data.map(record => record.uv)
    

  //   //     const options: Highcharts.Options = {
  //   //   chart: {
  //   //     type: 'bar'
  //   //   },
  //   //   title: {
  //   //     text: 'Il mio primo grafico Highcharts in Angular'
  //   //   },
  //   //   series: [{
  //   //     type: 'bar', // Tipo di grafico
  //   //     name: "Based on Rooms",
  //   //     data: formattedData.slice(0, 20),
  //   //   }]
  //   // };
  //   // return options;
  //   //const data = this.dataArray
  //   //console.log(this.housingMedianAge)
  //   return new Promise (resolve => {
  //     resolve(this.housingMedianAge = this.housingMedianAge.slice(0,20));
  // });
  // }

    // get houseMedianAgeChartsData() {
  //   const data = this.data?.map((record) => {
  //     return ({
  //       name: record['median_house_value'] ?? 0,
  //       uv: record['housing_median_age'] ?? 0
  //     });
  //   }) ?? [{ name: "", uv: 0 }];

  //   const formattedData = data.map(record => record.uv)

  //   const options: Highcharts.Options = {
  //     chart: {
  //       type: 'bar'
  //     },
  //     title: {
  //       text: 'Il mio primo grafico Highcharts in Angular'
  //     },
  //     series: [{
  //       type: 'bar', // Tipo di grafico
  //       name: "Based on Rooms",
  //       data: formattedData.slice(0, 20),
  //     }]
  //   };
  //   return options;
  // }
  fetchData(): Observable<Object> {
    return this.http.get(this.dataUrl)
      .pipe(catchError(this.errorHandler));
  }

  fetchSqlData(min: number, max: number): Observable<Object> {
    return this.http.get(`${this.dataUrl}?start=${Math.round(min)}&end=${Math.round(max)}`)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || "server error.");
  }

}



export class Data{
  longitude: number
  latitude: number
  housing_median_age:number
  total_rooms: number
  total_bedrooms: number
  population: number
  households: number
  median_income: number
  median_house_value: number
  ocean_proximity: string

  constructor(  longitude: number, latitude: number, housing_median_age:number, total_rooms: number,total_bedrooms: number,population: number,households: number,median_income: number,median_house_value: number,ocean_proximity: string){
    this.longitude = longitude;
    this.latitude = latitude;
    this.latitude =  latitude;
    this.housing_median_age = housing_median_age;
    this.total_rooms =  total_rooms;
    this.total_bedrooms =  total_bedrooms;
    this.population =  population;
    this.households =  households;
    this.median_income =  median_income;
    this.median_house_value =  median_house_value;
    this.ocean_proximity =  ocean_proximity;
  }
}

export class ChartData {
  name: number
  uv: number

  constructor(name: number, uv: number) {
    this.name = name;
    this.uv = uv;

  }
}