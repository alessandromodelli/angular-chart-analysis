import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';



@Component({
  selector: 'app-high-chart-pie',
  templateUrl: './high-chart-pie.component.html',
  styleUrls: ['./high-chart-pie.component.css']
})
export class HighChartPieComponent implements AfterViewInit, OnInit {

  //@Input() data: ChartData[] = [];

  public optionsChart: Highcharts.Options = {}

  public optionsChart1: Highcharts.Options = {}

  public optionsChart2: Highcharts.Options = {}

  public optionsChart3: Highcharts.Options = {}

  public data = [
    { value: 335, name: 'Direct' },
    { value: 310, name: 'Email' },
    { value: 274, name: 'Union Ads' },
    { value: 235, name: 'Video Ads' },
    { value: 400, name: 'Search Engine' }
  ];
  public title = "Traffic sources"

  ngOnInit(): void {

    this.optionsChart = {
      chart: {
        plotBackgroundColor: undefined,
        plotBorderWidth: undefined,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: this.title,
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Brands',
        data: this.data.map(data => { return { name: data.name, y: data.value } })
      }]
    }

    const colors = ["#2caffe", "#544fc5", "#00e272", "#fe6a35", "#6b8abc", "#d568fb"]

    this.optionsChart1 = {
      chart: {
        plotBackgroundColor: undefined,
        plotBorderWidth: undefined,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: this.title,
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          colors,
          borderRadius: 5,
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
            distance: -50,
            filter: {
              property: 'percentage',
              operator: '>',
              value: 4
            }
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Share',
        data: this.data.map(data => { return { name: data.name, y: data.value } })
      }]
    }

    this.optionsChart2 = {
      chart: {
        plotBackgroundColor: undefined,
        plotBorderWidth: undefined,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: this.title,
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{
        type: 'pie',
        name: 'Brands',
        data: this.data.map(data => { return { name: data.name, y: data.value } })
      }]
    }

    this.optionsChart3 = {
      chart: {
        plotBackgroundColor: undefined,
        plotBorderWidth: 0,
        plotShadow: false
      },
      title: {
        text: this.title,
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white'
            }
          },
          startAngle: -90,
          endAngle: 90,
          center: ['50%', '75%'],
          size: '110%'
        }
      },
      series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '50%',
        data: this.data.map(data => { return { name: data.name, y: data.value } })
      }]
    }
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color



  }

  ngAfterViewInit() {
    Highcharts.chart('pieChart', this.optionsChart);
    Highcharts.chart('pieChart1', this.optionsChart1);
    Highcharts.chart('pieChart2', this.optionsChart2);
    Highcharts.chart('pieChart3', this.optionsChart3);
  }
}