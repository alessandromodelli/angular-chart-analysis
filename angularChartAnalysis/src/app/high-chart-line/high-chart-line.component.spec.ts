import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighChartLineComponent } from './high-chart-line.component';

describe('HighChartLineComponent', () => {
  let component: HighChartLineComponent;
  let fixture: ComponentFixture<HighChartLineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HighChartLineComponent]
    });
    fixture = TestBed.createComponent(HighChartLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
