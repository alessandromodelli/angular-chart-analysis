import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ChartData } from '../high-chart/high-chart.component';

@Component({
  selector: 'app-high-chart-line',
  templateUrl: './high-chart-line.component.html',
  styleUrls: ['./high-chart-line.component.css']
})
export class HighChartLineComponent implements AfterViewInit, OnInit{

 // @Input() data: ChartData[] = [];

  public optionsChart: Highcharts.Options = {}

  public optionsChart1: Highcharts.Options = {}

  public optionsChart2: Highcharts.Options = {}

  public optionsChart3: Highcharts.Options = {}

  public data = [
    { name: "Jan", value: 28 },
    { name: "Feb", value: 29 },
    { name: "Mar", value: 33 },
    { name: "Apr", value: 36 },
    { name: "May", value: 23 },
    { name: "Jun", value: 23 },
    { name: "Jul", value: 28 }
];
// const years = ['2012', '2013', '2014', '2015', '2016'];
public title = 'Average High & Low Temperature'


  ngOnInit(): void {
    this.optionsChart = {

      title: {
          text: this.title,
          align: 'center'
      },
  
      yAxis: {
          title: {
              text: 'Temperature'
          }
      },
  
      xAxis: {
        categories: this.data.map(data => data.name)
      },
  
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
  
      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              },
              pointStart: 2010
          }
      },
  
      series: [{
        type: "line",
          name: '2013',
          data: this.data.map(data=> data.value)
      }],
  
      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  
  }

    this.optionsChart2 = {

        title: {
            text: this.title,
            align: 'center'
        },
    
        yAxis: {
            title: {
                text: 'Temperature'
            }
        },
    
        xAxis: {
            categories: this.data.map(data => data.name)
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            series: {
                step: 'center',
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },
    
        series: [{
          type: "line",
            name: '2013',
            data: this.data.map(data=> data.value)
        }],
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    };

    this.optionsChart1 = {
      chart: {
          type: 'line'
      },
      title: {
          text: this.title
      },
      xAxis: {
          categories: this.data.map(data => data.name)
      },
      yAxis: {
          title: {
              text: 'Temperature (°C)'
          }
      },
      plotOptions: {
          line: {
              dataLabels: {
                  enabled: true
              },
              enableMouseTracking: false
          }
      },
      series: [{
        type: 'line',
          name: '2013 - High',
          data: this.data.map(data => data.value)
      }, {
        type: 'line',
          name: '2013 - Low',
          data: this.data.map(data=> data.value - 10)
      }]
  }

    this.optionsChart3 = {
        chart: {
            type: 'line'
        },
        title: {
            text: this.title
        },
        xAxis: {
            categories: this.data.map(data => data.name)
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
          type: 'line',
            name: '2014',
            data: this.data.map(data => data.value)
        }, {
          type: 'line',
            name: '2015',
            data: this.data.map(data=> data.value + 10)
        }, {
            type: 'line',
              name: '2016',
              data: this.data.map(data=> data.value - 10)
          }]
    }

    
  }

  ngAfterViewInit() {
    Highcharts.chart('lineChart', this.optionsChart);
    Highcharts.chart('lineChart1', this.optionsChart1);
    Highcharts.chart('lineChart2', this.optionsChart2);
    Highcharts.chart('lineChart3', this.optionsChart3);
  }
}
