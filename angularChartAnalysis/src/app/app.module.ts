import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HighChartComponent } from './high-chart/high-chart.component';
import { FormsModule } from '@angular/forms';
import { FusionChartsModule } from "angular-fusioncharts";
import { HttpClientModule } from '@angular/common/http';
import { ReadCsvComponent } from './read-csv/read-csv.component';
import { HighChartBarComponent } from './high-chart-bar/high-chart-bar.component';
import { DataService } from './data-service';
import { HighChartLineComponent } from './high-chart-line/high-chart-line.component';
import { HighChartAreaComponent } from './high-chart-area/high-chart-area.component';
import { HighChartPieComponent } from './high-chart-pie/high-chart-pie.component';
import { FusionChartComponent } from './fusion-chart/fusion-chart.component';
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { FusionChartBarComponent } from './fusion-chart-bar/fusion-chart-bar.component';
import { FusionChartLineComponent } from './fusion-chart-line/fusion-chart-line.component';
import { FusionChartPieComponent } from './fusion-chart-pie/fusion-chart-pie.component';
import { FusionChartAreaComponent } from './fusion-chart-area/fusion-chart-area.component';
import { AnyChartComponent } from './any-chart/any-chart.component';
import { AnyChartBarComponent } from './any-chart-bar/any-chart-bar.component';
import { AnyChartLineComponent } from './any-chart-line/any-chart-line.component';
import { AnyChartPieComponent } from './any-chart-pie/any-chart-pie.component';
import { AnyChartAreaComponent } from './any-chart-area/any-chart-area.component';


FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    AppComponent,
    HighChartComponent,
    ReadCsvComponent,
    HighChartBarComponent,
    HighChartLineComponent,
    HighChartAreaComponent,
    HighChartPieComponent,
    FusionChartComponent,
    FusionChartBarComponent,
    FusionChartLineComponent,
    FusionChartPieComponent,
    FusionChartAreaComponent,
    AnyChartComponent,
    AnyChartBarComponent,
    AnyChartLineComponent,
    AnyChartPieComponent,
    AnyChartAreaComponent
  ],
  imports: [
    BrowserModule,FusionChartsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
