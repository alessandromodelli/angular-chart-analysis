import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighChartAreaComponent } from './high-chart-area.component';

describe('HighChartAreaComponent', () => {
  let component: HighChartAreaComponent;
  let fixture: ComponentFixture<HighChartAreaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HighChartAreaComponent]
    });
    fixture = TestBed.createComponent(HighChartAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
