import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    selector: 'app-high-chart-area',
    templateUrl: './high-chart-area.component.html',
    styleUrls: ['./high-chart-area.component.css']
})
export class HighChartAreaComponent implements AfterViewInit, OnInit {

    //@Input() data: ChartData[] = [];

    public optionsChart: Highcharts.Options = {}

    public optionsChart1: Highcharts.Options = {}

    public optionsChart2: Highcharts.Options = {}

    public optionsChart3: Highcharts.Options = {}

    public data = [
        { name: "Jan", value: 28 },
        { name: "Feb", value: 29 },
        { name: "Mar", value: 33 },
        { name: "Apr", value: 36 },
        { name: "May", value: 23 },
        { name: "Jun", value: 23 },
        { name: "Jul", value: 28 }
    ];
    // const years = ['2012', '2013', '2014', '2015', '2016'];
    public title = 'Average High & Low Temperature'


    ngOnInit(): void {
        this.optionsChart = {
            chart: {
                type: 'area'
            },
            title: {
                text: this.title
            },
            xAxis: {
                categories: this.data.map(data => data.name)
            },
            yAxis: {
                title: {
                    text: 'Temperature'
                }
            },
            tooltip: {
                pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                type: 'area',
                name: '2013',
                data: this.data.map(data => data.value)
            }]
        }

        this.optionsChart1 = {
            chart: {
                type: 'area'
            },
            title: {
                text: this.title
            },
            yAxis: {
                labels: {
                    format: '{value}%'
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.1f} billion Gt)<br/>',
                split: true
            },
            series: [{
                type: 'area',
                name: '2013 - High',
                data: this.data.map(data => data.value)
            }, {
                type: 'area',
                name: '2013 - Low',
                data: this.data.map(data => data.value - 10)
            }]
        }

        this.optionsChart2 = {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: this.title,

            },
            legend: {
                layout: 'vertical',
                verticalAlign: 'top',
                x: 120,
                y: 70,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF'
            },
            xAxis: {
                categories: this.data.map(data => data.name)
            },
            yAxis: {
                title: {
                    text: 'Temperature'
                }
            },
            tooltip: {
                shared: true,
                headerFormat: '<b>Hunting season starting autumn {point.x}</b><br>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointStart: 2000
                },
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [{
                type: 'areaspline',
                name: '2013',
                data: this.data.map(data => data.value + 20)

            }, {
                type: 'areaspline',
                name: '2014',
                data: this.data.map(data => data.value + 10)

            }, {
                type: 'areaspline',
                name: '2015',
                data: this.data.map(data => data.value)

            }, {
                type: 'areaspline',
                name: '2016',
                data: this.data.map(data => data.value - 10)

            }]
        }

        this.optionsChart3 = {
            chart: {
                type: 'area'
            },
            title: {
                text: this.title,
            },

            xAxis: {
                categories: this.data.map(data => data.name)
            },
            yAxis: {
                title: {
                    text: 'Temperature'
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'area',

                name: '2014',
                step: "center",
                data: this.data.map(data => data.value + 20)
            }, {
                type: 'area',

                name: '2015',
                step: "center",
                data: this.data.map(data => data.value + 10)
            }, {
                type: 'area',

                name: '2016',
                step: "center",
                data: this.data.map(data => data.value )
            }]
        }


    }

    ngAfterViewInit() {
        Highcharts.chart('areaChart', this.optionsChart);
        Highcharts.chart('areaChart1', this.optionsChart1);
        Highcharts.chart('areaChart2', this.optionsChart2);
        Highcharts.chart('areaChart3', this.optionsChart3);
    }
}
